API
===

TODO: basic/digest/session cookie/OAuth authenticated requests should all get more
lenient limits, but how is the rate limiter to know?


The rate limiter adds the following headers to a response:

    X-RateLimit-Limit: 900
    X-RateLimit-Remaining: 899
    X-RateLimit-Reset: 900

- **X-RateLimit-Limit** The total number of requests you can make within a
    particular time period.
- **X-RateLimit-Remaining** The number of requests remaining for this time period.
- **X-RateLimit-Reset** The remaining seconds before the time period resets.


If you run out of requests the server will respond like this:

    $ curl -i https://example.com/webservice/

    HTTP/1.1 429 Too Many Requests
    Status: 429 Too Many Requests
    X-RateLimit-Limit: 900
    X-RateLimit-Remaining: 0
    X-RateLimit-Reset: 123


You can always check the rate limit without incurring an API hit:

    $ curl -i https://example.com/rate-limit/

    HTTP/1.1 200 OK
    Status: 200 OK
    X-RateLimit-Limit: 900
    X-RateLimit-Remaining: 0
    X-RateLimit-Reset: 120

    {
        "rate": {
            "limit": 900
            "remaining": 0,
            "reset": 120,
        }
    }

