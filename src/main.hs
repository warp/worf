{-# LANGUAGE OverloadedStrings #-}
module Main where

--  A warp is learning haskell experiment :)
--  Copyright (C) 2013  Kuno Woudt <kuno@frob.nl>
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU Affero General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU Affero General Public License for more details.
--
--  You should have received a copy of the GNU Affero General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import qualified Control.Concurrent.STM as STM
import           Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString.UTF8 as UTF8
import qualified Network.HTTP.Conduit as Conduit
import qualified Network.HTTP.ReverseProxy as RevProxy
import qualified Network.HTTP.Types.Header as Hdr
import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Middleware.RequestLogger as RequestLogger

type Limit = STM.TVar Integer

newLimit :: Integer -> IO Limit
newLimit amount = STM.newTVarIO amount

main :: IO ()
main = do
  putStrLn "Listening on http://0.0.0.0:4080/"
  manager <- Conduit.newManager Conduit.def
  limit <- newLimit 900
  Warp.run 4080 $ RequestLogger.logStdout $ addHeaders (rateLimiter limit) $ app manager

app :: Conduit.Manager -> Wai.Application
app = RevProxy.waiProxyTo
          (const $ return $ Right $ RevProxy.ProxyDest "localhost" 80)
          RevProxy.defaultOnExc

addHeaders :: IO Hdr.ResponseHeaders -> Wai.Middleware
addHeaders headers app req = do
  extra_headers <- liftIO headers
  res <- app req
  case res of
    Wai.ResponseFile s hs fpath fpart -> return $ Wai.ResponseFile s (hs ++ extra_headers) fpath fpart
    Wai.ResponseBuilder s hs bldr -> return $ Wai.ResponseBuilder s (hs ++ extra_headers) bldr
    Wai.ResponseSource s hs src -> return $ Wai.ResponseSource s (hs ++ extra_headers) src

rateLimiter :: Limit -> IO Hdr.ResponseHeaders
rateLimiter limit = do
  remaining <- STM.atomically $ STM.readTVar limit
  STM.atomically $ STM.writeTVar limit (remaining - 1)
  return [("X-RateLimit-Remaining", UTF8.fromString $ show remaining)]
