
Install
=======

worf needs Haskell, on a recent Ubuntu you can install it like this:

    sudo apt-get install haskell-platform

It also needs a bunch of haskell packages which are not provided by
the haskell platform or need newer versions than those provided by the
haskell platform.  The following steps should install all the required
packages and then build worf.

    cabal update
    cabal install --only-dependencies
    cabal configure
    cabal build


API
===

See API.md
